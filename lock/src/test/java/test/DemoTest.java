package test;

import com.coderdz.lock.DistributedLockApp;
import com.coderdz.lock.service.StockService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.annotation.Resource;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @projectName: distributed-locks
 * @description: 测试
 * @date: 2023/11/8 15:06
 * @author: dingzhen
 */
@SpringBootTest(classes = DistributedLockApp.class)
@Slf4j
public class DemoTest {

    @Resource
    private StockService stockService;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Test
    public void testDeductStock() {
        stockService.deductByRedis();
    }

    @Test
    public void testTimer() {
        log.info("定时任务初始时间: " + System.currentTimeMillis() / 1000);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                log.info("定时任务执行时间: " + System.currentTimeMillis() / 1000);
            }
        }, 2000, 3000);
    }

    @Test
    public void testRedisConnect() {
        stringRedisTemplate.opsForValue().set("test", "test");
        String s = stringRedisTemplate.opsForValue().get("test");
        assert s != null;
        log.info("test data : " + s);
        stringRedisTemplate.delete(s);
    }

}
