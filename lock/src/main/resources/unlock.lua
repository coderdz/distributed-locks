-- 释放锁逻辑
-- 比较锁中的标识是否和当前线程id一致
-- 一致则释放，否则不释放
if redis.call('get', KEYS[1]) == ARGV[1] then
    return redis.call('del', KEYS[1])
end
return 0