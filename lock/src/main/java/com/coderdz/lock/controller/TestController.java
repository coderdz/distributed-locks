package com.coderdz.lock.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @projectName: distributed-locks
 * @description: 接口测试
 * @date: 2023/11/10 15:49
 * @author: dingzhen
 */
@RestController
@RequestMapping("test")
public class TestController {

    @GetMapping("ping")
    public String ping() {
        return "pong";
    }

}
