package com.coderdz.lock.controller;

import com.coderdz.lock.service.StockService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @projectName: distributed-locks
 * @description:
 * @date: 2023/11/7 10:53
 * @author: dingzhen
 */
@RestController
@RequestMapping("stock")
@Slf4j
public class StockController {

    @Resource
    private StockService stockService;

    /**
     * 扣减库存
     * @return
     */
    @GetMapping("deduct")
    public String deduct() {
        stockService.deductByRedisLockClient();
        return "deduct success";
    }

}
