package com.coderdz.lock.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coderdz.lock.pojo.Stock;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @projectName: distributed-locks
 * @description:
 * @date: 2023/11/7 14:28
 * @author: dingzhen
 */
public interface StockMapper extends BaseMapper<Stock> {

    /**
     * 更新库存
     * @param count 每次扣减数量
     * @param productCode 商品id
     * @return
     */
    @Update("update db_stock set count = #{count} - 1 where product_code = #{productCode} and count >= ${count}")
    int updateStock(@Param("productCode") String productCode, @Param("count") Integer count);

    /**
     * 使用mysql当前读来获取最新库存记录
     * @param productCode
     * @return
     */
    @Select("select * from db_stock where product_code = #{productCode} for update")
    List<Stock> queryStock(String productCode);

}
