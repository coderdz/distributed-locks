package com.coderdz.lock.lock;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * @projectName: distributed-locks
 * @description: 分布式锁客户端工具类
 * @date: 2023/11/20 10:07
 * @author: dingzhen
 */
@Component
public class DistributedLockClient {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    private String uuid;

    public DistributedLockClient() {
        uuid = UUID.randomUUID().toString().replaceAll("-", "");
    }

    public DistributedRedisLock getRedisLock(String lockName) {
        return new DistributedRedisLock(stringRedisTemplate, lockName, uuid);
    }

}
