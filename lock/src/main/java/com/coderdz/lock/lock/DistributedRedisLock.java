package com.coderdz.lock.lock;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * @projectName: distributed-locks
 * @description: 分布式锁工具类,提供加锁解锁方法,redis锁结构 hash{lockKey:{uuid+threadId: value(重入次数)}}
 * @date: 2023/11/14 14:2
 * @author: dingzhen
 */
@Slf4j
public class DistributedRedisLock implements Lock {

    private StringRedisTemplate stringRedisTemplate;

    /**
     * 锁的key
     */
    private final String lockName;

    /**
     * 锁的field
     */
    private String uuid;

    /**
     * 锁的过期时间
     */
    private long expire = 30;

    public DistributedRedisLock(StringRedisTemplate stringRedisTemplate, String lockName, String uuid) {
        this.stringRedisTemplate = stringRedisTemplate;
        this.lockName = lockName;
        this.uuid = uuid + ":" + Thread.currentThread().getId();
    }

    @Override
    public void lock() {
        tryLock();
    }

    @Override
    public boolean tryLock() {
        try {
            return this.tryLock(-1L, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 核心加锁方法
     * @param time 加锁时间
     * @param unit 时间单位
     * @return 加锁处理结果
     * @throws InterruptedException
     */
    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        if (time != -1){
            this.expire = unit.toSeconds(time);
        }
        // 加锁脚本
        String script = "if redis.call('exists', KEYS[1]) == 0 or " +
                            "redis.call('hexists', KEYS[1], ARGV[1]) == 1 " +
                        "then " +
                            "redis.call('hincrby', KEYS[1], ARGV[1], 1) " +
                            "redis.call('expire', KEYS[1], ARGV[2]) " +
                            "return 1 " +
                        "else " +
                            "return 0 " +
                        "end";
        log.info("tryLock() 加锁 lockKey:{}, uuid:{}, expire:{}", lockName, uuid, expire);
        while (Boolean.FALSE.equals(stringRedisTemplate.execute(new DefaultRedisScript<>(script, Boolean.class),
                Collections.singletonList(lockName), uuid, String.valueOf(expire)))) {
            Thread.sleep(50);
        }
        // 加锁成功后, 通过定时器自动对锁进行续期
        renewExpire();
        return true;
    }

    /**
     * 解锁方法
     */
    @Override
    public void unlock() {
        String script = "if redis.call('hexists', KEYS[1], ARGV[1]) == 0 " +
                        "then " +
                            "return nil " +
                        "elseif redis.call('hincrby', KEYS[1], ARGV[1], -1) == 0 " +
                        "then " +
                            "return redis.call('del', KEYS[1]) " +
                        "else " +
                            "return 0 " +
                        "end";
        Long flag = stringRedisTemplate.execute(new DefaultRedisScript<>(script, Long.class),
                Collections.singletonList(lockName), uuid);
        if (flag == null) {
            log.error("unlock() error: this lock doesn't belong to you, lockName:{}, field:{}", lockName, uuid);
            throw new IllegalMonitorStateException("this lock doesn't belong to you");
        }
        log.info("unlock() 解锁 lockKey:{}, uuid:{}", lockName, uuid);
    }

    /**
     * 定时任务给锁重新续期,防止业务逻辑没执行完锁就过期了
     * 判断当前lockKey是否存在(hash结构), 存在则续期
     * 不存在则返回0(false), 则什么也不做
     */
    private void renewExpire() {
        String script = "if redis.call('hexists', KEYS[1], ARGV[1]) == 1 " +
                        "then " +
                            "return redis.call('expire', KEYS[1], ARGV[2]) " +
                        "else " +
                            "return 0 " +
                        "end";
        // delay: 延迟时间; period: 定时任务执行间隔时间
        log.info("renewExpire() 开始准备续期 lockKey:{}, uuid:{}, expire:{}", lockName, uuid, expire);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if (Boolean.TRUE.equals(stringRedisTemplate.execute(new DefaultRedisScript<>(script, Boolean.class),
                        Collections.singletonList(lockName), uuid, String.valueOf(expire)))) {
                    log.info("renewExpire()续期成功! lockKey:{}, uuid:{}, expire:{}", lockName, uuid, expire);
                    log.info("renewExpire() {}ms后定时器继续检查续期", expire * 1000 / 3);
                    renewExpire();
                }
            }
        }, expire * 1000 / 3);
    }

    @Override
    public Condition newCondition() {
        return null;
    }

    @Override
    public void lockInterruptibly() {

    }

}
