package com.coderdz.lock.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @projectName: distributed-locks
 * @description: 库存
 * @date: 2023/11/7 10:43
 * @author: dingzhen
 */
@Data
@TableName("db_stock")
public class Stock {

    private Long id;

    /**
     * 产品编号
     */
    private String productCode;

    /**
     * 仓库
     */
    private String wareHouse;

    /**
     * 数量
     */
    private Integer count;

    /**
     * 版本
     */
    private Integer version;

}
