package com.coderdz.lock.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.coderdz.lock.lock.DistributedLockClient;
import com.coderdz.lock.lock.DistributedRedisLock;
import com.coderdz.lock.mapper.StockMapper;
import com.coderdz.lock.pojo.Stock;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @projectName: distributed-locks
 * @description: 扣减库存service
 * @date: 2023/11/7 10:44
 * @author: dingzhen
 */
@Service
@Slf4j
// 多例bean
//@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class StockService {

    @Resource
    private StockMapper stockMapper;

    @Resource
    private RedissonClient redissonClient;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    private final ReentrantLock lock = new ReentrantLock();

    private final String stockKey = "stock";

    private final String lockKey = "deduct:lock";

    @Resource
    private DistributedLockClient distributedLockClient;

    /**
     * 基于JVM锁实现扣减库存
     */
//    @Transactional(isolation = Isolation.REPEATABLE_READ, rollbackFor = Exception.class)
    public void deductStock() {
        lock.lock();
        try {
            QueryWrapper<Stock> wrapper = new QueryWrapper<>();
            wrapper.select("id", "count");
            wrapper.eq("product_code", "1001");
            Stock stock = stockMapper.selectOne(wrapper);
            if (stock != null && stock.getCount() > 0) {
                stock.setCount(stock.getCount() - 1);
                stockMapper.updateById(stock);
                log.info("deductStock() 库存余量: " + stock.getCount());
            }
        } catch (Exception e) {
            log.error("deductStock() error: " + e);
        } finally {
            lock.unlock();
        }
    }

    /**
     * 使用自定义sql语句实现库存扣减
     * update/delete/insert 写操作本身会加锁
     */
    public void deductByMysqlLock() {
        try {
            stockMapper.updateStock("1001", 1);
        } catch (Exception e) {
            log.error("deductByMysqlLock() error: " + e);
        }
    }

    /**
     * 使用当前读
     */
    @Transactional(rollbackFor = Exception.class)
    public void deductForUpdate() {
        try {
            // 1. 查询库存
            List<Stock> stocks = stockMapper.queryStock("1001");
            // 2. 选择库存
            Stock stock = stocks.get(0);
            // 3. 判断库存是否充足
            if (stock != null && stock.getCount() > 0) {
                stock.setCount(stock.getCount() - 1);
                stockMapper.updateById(stock);
                log.info("deductForUpdate() 库存余量: " + stock.getCount());
            }
        } catch (Exception e) {
            log.error("deductForUpdate() error: " + e);
        }
    }

    /**
     * mysql乐观锁
     * 通过给表增加version/时间戳字段实现
     * 类似CAS原理解决扣减库存并发问题
     */
    public void deductByLuckyLock() {
        try {
            // 1. 查询库存
            List<Stock> stocks = stockMapper.queryStock("1001");
            // 2. 选择库存
            Stock stock = stocks.get(0);
            // 3. 判断库存是否充足
            if (stock != null && stock.getCount() > 0) {
                stock.setCount(stock.getCount() - 1);
                Integer version = stock.getVersion();
                stock.setVersion(version + 1);
                if (stockMapper.update(stock, new UpdateWrapper<Stock>().eq("id", stock.getId())
                        .eq("version", stock.getVersion())) == 0) {
                    Thread.sleep(20);
                    deductByLuckyLock();
                }
            }
        } catch (Exception e) {
            log.error("deductByLuckyLock() error : " + e);
        }
    }

    /**
     * 基于redis实现分布式锁
     */
    public void deductByRedis() {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        String threadId = uuid + "-" + Thread.currentThread().getId();
        // 加锁
        if (Boolean.FALSE.equals(stringRedisTemplate.opsForValue().setIfAbsent(lockKey, threadId, 3, TimeUnit.SECONDS))) {
            try {
                Thread.sleep(50);
                deductByRedis();
            } catch (InterruptedException e) {
                log.error("deductByRedis() error: " + e);
            }
        }
        // 抢到锁了
        try {
            String stock = stringRedisTemplate.opsForValue().get(stockKey);
            if (stock != null && !stock.isEmpty()) {
                int st = Integer.parseInt(stock);
                if (st > 0) {
                    stringRedisTemplate.opsForValue().set("stock", String.valueOf(--st));
                }
            }
        } catch (Exception e) {
            log.error("deductByRedis() error: " + e);
        } finally {
            // 解锁(判断是否是自己的锁)
            if (StringUtils.equals(threadId, stringRedisTemplate.opsForValue().get(lockKey))) {
                stringRedisTemplate.delete(lockKey);
            }
        }
    }

    /**
     * redis+lua脚本保证原子性
     */
    public void deductByRedisLua() {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        String threadId = uuid + "-" + Thread.currentThread().getId();
        // 加锁
        if (Boolean.FALSE.equals(stringRedisTemplate.opsForValue().setIfAbsent(lockKey, threadId))) {
            try {
                Thread.sleep(50);
                deductByRedisLua();
            } catch (InterruptedException e) {
                log.error("deductByRedis() error: " + e);
            }
        }
        // 抢到锁了
        try {
            String stock = stringRedisTemplate.opsForValue().get(stockKey);
            if (stock != null && !stock.isEmpty()) {
                int st = Integer.parseInt(stock);
                if (st > 0) {
                    stringRedisTemplate.opsForValue().set("stock", String.valueOf(--st));
                }
            }
        } catch (Exception e) {
            log.error("deductByRedis() error: " + e);
        } finally {
            // 定义lua脚本
            String script = "if redis.call('get', KEYS[1]) == ARGV[1]" +
                    "then " +
                    "    return redis.call('del', KEYS[1]) " +
                    "else return 0 end";
            stringRedisTemplate.execute(new DefaultRedisScript<>(script, Boolean.class),
                    Collections.singletonList(lockKey), threadId);
        }
    }

    /**
     * 使用自定义redis分布式锁客户端工具类实现库存扣减
     * 内部使用redis+lua脚本实现
     * @see com.coderdz.lock.lock.DistributedRedisLock
     */
    public void deductByRedisLockClient() {
        DistributedRedisLock redisLock = distributedLockClient.getRedisLock(lockKey);
        redisLock.lock();
        try {
            // 先查询库存是否充足
            String stock = stringRedisTemplate.opsForValue().get(stockKey);
            // 再减库存
            if (stock != null && !stock.isEmpty()) {
                int st = Integer.parseInt(stock);
                if (st > 0) {
                    stringRedisTemplate.opsForValue().set(stockKey, String.valueOf(--st));
                    log.info("库存扣减成功, 剩余:" + st);
                }
            }
            Thread.sleep(10000);
//            testReentrantLock();
        } catch (Exception e) {
            log.error("unlock() error: " + e);
        } finally {
            redisLock.unlock();
        }
    }

    /**
     * 测试自定义redis分布式锁的可重入性
     */
    public void testReentrantLock() {
        DistributedRedisLock redisLock = distributedLockClient.getRedisLock(lockKey);
        redisLock.lock();
        log.info("testReentrantLock() 测试redis分布式锁的可重入性");
        redisLock.unlock();
    }

    /**
     * 使用Redisson框架实现分布式锁, 扣减库存
     */
    public void deductByRedisson() {
        // 加锁，获取锁失败重试
        RLock lock = this.redissonClient.getLock(lockKey);
        lock.lock();
        try {
            // 先查询库存是否充足
            String stock = stringRedisTemplate.opsForValue().get(stockKey);
            // 再减库存
            if (stock != null && !stock.isEmpty()) {
                int st = Integer.parseInt(stock);
                if (st > 0) {
                    stringRedisTemplate.opsForValue().set("stock", String.valueOf(--st));
                    log.info("库存扣减成功, 剩余:" + st);
                }
            }
        } catch (Exception e) {
            log.error("deductByRedisson() error : " + e);
        } finally {
            // 释放锁
            lock.unlock();
        }
    }


}
