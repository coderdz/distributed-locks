package com.coderdz.lock;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @projectName: distributed-locks
 * @description: 主启动类
 * @date: 2023/11/7 10:38
 * @author: dingzhen
 */
@SpringBootApplication
@MapperScan("com.coderdz.lock.mapper")
public class DistributedLockApp {
    public static void main(String[] args) {
        SpringApplication.run(DistributedLockApp.class, args);
    }
}
