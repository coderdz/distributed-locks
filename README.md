# 分布式锁

#### 介绍
分布式锁全套解决方案,以扣减商品库存为例


| 环境         |                      jdk1.8、maven3.x                       |
| ------------ | :---------------------------------------------------------: |
| **技术点**   | SpringBoot2.x、Mybatis-Plus、SpringData-Redis、MySQL、Redis |
| **开发工具** |                     idea、nginx、jmeter                     |
| **其他**     |                     Zookeeper、Lua、JUC                     |

#### 分布式锁解决方案

1.  JVM本地锁
2.  MySQL悲观锁、乐观锁
3.  Redis + lua、Redisson
4.  Zookeeper
5.  Curator

#### 参与贡献

1.  Fork 本仓库 develop分支
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request 到develop分支


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
